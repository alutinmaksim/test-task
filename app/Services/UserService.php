<?php


namespace App\Services;


use App\Http\Resources\UserResource;
use App\Models\User;

class UserService
{

    public function save($request)
    {
        $data = $request->validated();
        $user = User::create($data);
        return new UserResource($user);
    }

    public function getById($id)
    {
        $user = User::query()->findOrFail($id);
        return new UserResource($user);
    }
}
