<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckHeaderIsValid
{
    public function handle(Request $request, Closure $next)
    {
        $response = response()->json('У пользователя нет прав доступа к содержимому', 403);

        if (! $request->hasHeader('User-Id')) {
            return $response;
        }

        $headerId = $request->header('User-Id');
        if ($headerId != $request->user->id) {
            return $response;
        }

        return $next($request);
    }
}
